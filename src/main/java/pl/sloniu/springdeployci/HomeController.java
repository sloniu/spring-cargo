package pl.sloniu.springdeployci;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {


    @GetMapping("/")
    @ResponseBody
    public String home() {
        return "Welcome in webapp deployed by Jenkins !!!UPDATED3!!!";
    }

    @GetMapping("/get")
    @ResponseBody
    public String get() {
        return "Something";
    }

    @GetMapping("/get/")
    @ResponseBody
    public String get2(@RequestParam String name) {
        return "Tomek Ty stary rowerze xd";
    }

    @GetMapping("/welcome/{name}")
    @ResponseBody
    public String welcome(@PathVariable String name) {
        return name + " Ty stary rowerze, za robotę się weź xdd";
    }

     @GetMapping("/welcome2/{name}")
    @ResponseBody
    public String welcome2(@PathVariable String name) {
        return name + " stary pijany";
    }




}
